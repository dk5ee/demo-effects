
Build Instructions for debian

Install dependencies:

apt install autoconf automake dh-autoreconf libltdl-dev libsdl-dev libsdl-image1.2-dev freeglut3-dev

Prepare:

autoreconf -f -i .

./configure

Compile:

Unfortunatly there is a mishmash of c++ and c code in WPCG, some pointer casts warnings and more are thrown, and some effects result in segfaults

make

Tip:

if error message appears:

*** No rule to make target 'xxx.c', needed by 'xxx.o'.  Stop.

then delete the .depts directory in the subfolder
