This is an import of the GPL Project by W.P. van Paassen

## The Demo Effect Collection

in short: TDEC

Original project site: http://demo-effects.sourceforge.net/



=========================================

## Original Description

This is not only a collection of demo effects from the early days of the demo scene (for instance the Amiga demo scene in the late eighties ) but also a multi-layered run-time pluggable demo effects system, with support for filters and transition effects. which can be used to create demo's.

The effects are currently programmed in C/C++ and should build on any Unix flavoured OS.

The SDL library is used for video access, the SDL_image library is used for loading graphics, Mikmod is used for music (optional). OPCODE was used for 3D frustum culling and 3D collision detection. OPENGL is used for the 3D effects.

The Demo Effects Collection is licensed under the GPL.

Feel free to send me your favourite effect/plugin!  Or be brave and join as a developer.

Thanks to Nupur Saurabh for the great banner and logos!

Tully Hillenbrand has packed some windows binaries for you. Get it here.

By the way It should be possible to compile TDEC on win32 with cygwin.


At the moment the following 34 effects are part of version 0.7.1

### Real-time Plasma
This is one of the most famous and most used effects. This is the real-time version, everything is calculated in the loop, so no color-cycled picture. The source code is heavily optimized, because this effect just has got to be fast people.

### Fire
Smoking! Such a simple and yet beautiful effect. Tweak the source to suite your personal taste.

### Particle Explosion
Particles are a real blast. This little effect uses 500 fire particles to create a big explosion. Press the space bar to relive the experience over and over again.

### 3D Starfield
This was a very popular effect in the early days of the demo scene.

### Copperbars
Probably one of the first demo effects. Nothing fancy, just added for completion.
 
### Candy Bar
Remember this  sweet Amiga effect?

### Sine Scroller Variations
I love these effects. Changes an ordinary scroller into something special.

### Shadebob
It's nice to see that these 'retro' effects bring back memories for more people. This shadebob effect was sent in by Byron Ellacot. Enjoy!

### Lens
Another cool effect by Byron Ellacot.  Watch the lens move over Tux. This is Retro!

### Jumping Scroller
A variation on a simple scroller. Very popular effect in the beginning of the Amiga demo scene

### Bobs
Another eye-catcher! (Don't you just love those cheap comments, I do :-) ). Almost every demo in the early days had at least some blitter objects moving about the screen.

### Unlimited Bobs
This effect raised quite a stir when it was released, endless number of bobs on the screen, impossible!?
Ofcourse it is all a trick. Can you figure it out?

### Rotozoom
Another nice effect sent in by Byron Ellacot. It will spin your head!
I optimized it for speed, check it out.

### Equalizer
Remember those Amiga demo's with the  4 copperbar equalizers. This is it!
It is also the first effect with music (Soundtracker mod) using the great mikmod sound library.

### Circle Scroller
Another variation of a scroller effect. I've always loved this version. I've only seen it in a demo or two but it blew me away! You'll be blown away too if you see how simple it really is. Again a proof that nice effects don't have to be hard, you've only got to think of it. Enough rambling, just check it out.

### SineWave
Sine waves have always been a major part of demos.  Watch tux get sine distorted in various ways. I threw in some palette fading too for that extra touch.
sinewave

### Flipping Image
Again a very popular effect. I've seen hundreds of logo's and images flipping about the screen. Remember the pouring effect which was used to put up an image? I've added that too, for getting in the right mood. Join me in having some nice demo memories.

### Water/Rain
This lovely effect has a major part in demo history, such a great looking effect,  indeed worth coding and watching.
I went for speed in this version. The great logo was done by Jason Liszkiewicz of the Creative Invasion Workshop,  thanks!

### Doom Melt
Ed Sinjiashvili was brave enough (are you?) to send me one of his favourite effects. Nice!
Do we all remember DOOM?  I sure do.

### Spiral Twist
Poor Tux, This effect is again distorting him? horribly spiral wise.

### 2D Bump Mapping
What is there to say about this effect? It's just plain beautifull!!

### 3D Key Frame Animated Quake 2 Model
After all these nice 2D effects it's about time to explore 3D too. This is the first real 3D effect and the first C++ effect of this collection.
Keep tuned for more stunning 3D effects coming to your linux box soon!

### 3D Terrain
This little demonstration shows an animated quake2 model centered in a vertex lit terrain surrounded by a skybox. The camera is attached to the model which is rotating in the scene.

### UFOS
This 3D demonstration shows the use of Pierre Terdiman's excellent 3D Collision detection library called OPCODE. It is used in this demo for both 3D frustum culling and the collisions among the ufos. This demonstration also shows improved terrain rendering and WPCG's new lighting system.
ufos 

### PlasmaCube
3D plasma effect!

### 3D Tunnel
This one is for my friend Edwin R. from Stockholm Sweden. Enjoy!

### 2D Blob
Amazing effect. Try to quit this fascinating effect!

### DEMONS
This new effect shows the latest features of the WPCG 3D library which are among others 3D object picking and object instancing. Objects can now share the same 3D and collision model with independent animation and collision detection.

### RUN-TIME PLUGGABLE MULTI-LAYERED DEMO EFFECT SYSTEM
TDEC has been extended with a run-time pluggable multi-layered demo effects plugin system, which is able to show
a lot of effects, filters and transition plugins concurrently and can be used to create demo's.

Check out the new MULTILAYER demo to see the system in action.

### RAYTRACER
The WPCG 3D library was extended with raytracing support.  This new demo reflects it all!

Enjoy!

Cheers,

W.P. van Paassen - 2005 -


